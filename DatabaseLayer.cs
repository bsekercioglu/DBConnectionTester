﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Globalization;
/// <summary>
/// Summary description for DatabaseLayer
/// </summary>
namespace Database
{
public static class DatabaseLayer
{
   
    public static String MSSQLBAGLANTI = "";
      
    public static SqlDataReader SQLdataread(string sql, string BaglantiCumlesi, string VeritabaniTipi)
  
    {
        
        SqlCommand cmdSQL = null;
        SqlConnection conSQL = null;
        SqlDataReader dtrSQL = null;
        string selectString = string.Empty;
        switch (VeritabaniTipi)
        {
                case "MSSQL":
                try
                {
                    conSQL = new SqlConnection(BaglantiCumlesi);
                    selectString = sql;
                    cmdSQL = new SqlCommand(selectString, conSQL);
                    conSQL.Open();
                    dtrSQL = cmdSQL.ExecuteReader(CommandBehavior.CloseConnection);
                    break;
                }
                catch (Exception )
                {
                 
                    break;
                   
                }
                  

                   
               
               

        }
        switch (VeritabaniTipi)
        {
            case "MSSQL":
                return dtrSQL;
            default:
                return null;
        }
       
       
    }
    public static DataTable dataset_aktar(string sql, string BaglantiCumlesi, string VeritabaniTipi)
    {
        System.Data.DataTable dtableSQL = new System.Data.DataTable();
        SqlConnection conSQL = new SqlConnection(BaglantiCumlesi);
        switch (VeritabaniTipi)
        {
            case "MSSQL":
                try
                {

                    conSQL.Open();
                    SqlDataAdapter DaSQL = new SqlDataAdapter(sql, conSQL);
                    DaSQL.SelectCommand.ExecuteNonQuery();
                    DaSQL.Fill(dtableSQL);
                    conSQL.Close();
                    conSQL.Dispose();
                    break;
                }
                catch (Exception )
                {

                       break;
                }
                finally
                {
                    SqlConnection.ClearPool(conSQL);
                    SqlConnection.ClearAllPools();

                }

            case "Oracle":
                break;

        }
        return dtableSQL;

    }
    public static DataTable datatable(string sql, string BaglantiCumlesi, string VeritabaniTipi)
    {
        System.Data.DataTable dtableSQL = new System.Data.DataTable();
        SqlConnection conSQL = new SqlConnection(BaglantiCumlesi);
        switch (VeritabaniTipi)
        {
            case "MSSQL":
                try
                {

                    conSQL.Open();
                    SqlDataAdapter DaSQL = new SqlDataAdapter(sql, conSQL);
                    DaSQL.SelectCommand.ExecuteNonQuery();
                    DaSQL.Fill(dtableSQL);
                    conSQL.Close();
                    conSQL.Dispose();
                    break;
                }
                catch (Exception )
                {

                    break;
                }
                finally
                {
                    SqlConnection.ClearPool(conSQL);
                    SqlConnection.ClearAllPools();

                }

            case "Oracle":
                break;

        }
        return dtableSQL;

    }
    public static DataSet dataset(string sql, string BaglantiCumlesi, string VeritabaniTipi)
    {
        System.Data.DataSet dtableSQL = new System.Data.DataSet();
        SqlConnection conSQL = new SqlConnection(BaglantiCumlesi);
        switch (VeritabaniTipi)
        {
            case "MSSQL":
                try
                {

                    conSQL.Open();
                    SqlDataAdapter DaSQL = new SqlDataAdapter(sql, conSQL);
                    DaSQL.SelectCommand.ExecuteNonQuery();
                    DaSQL.Fill(dtableSQL);
                    conSQL.Close();
                    conSQL.Dispose();
                    break;
                }
                catch (Exception )
                {

                      break;
                }
                finally
                {
                    SqlConnection.ClearPool(conSQL);
                    SqlConnection.ClearAllPools();

                }

            case "Oracle":
                break;

        }
        return dtableSQL;

    }
    public static void ExecuteSQL(string sql, string BaglantiCumlesi, string VeritabaniTipi)
    {
        DataTable dtable = new System.Data.DataTable();
        try
        {
            switch (VeritabaniTipi)
            {
                case "MSSQL":
                    SqlConnection connSQL = new SqlConnection(BaglantiCumlesi);
                    connSQL.Open();
                    SqlDataAdapter DASQL = new SqlDataAdapter(sql, connSQL);
                    DASQL.SelectCommand.ExecuteNonQuery();
                    connSQL.Close();
                    connSQL.Dispose();
                    break;
                case "Oracle":

                    break;
            }
        }
        catch (Exception )
        {

        //    Functions.Dosya.Logger("Hatalı İşlem :" +ex.Message+ Environment.NewLine + "SQL:" + sql);
        }
       
    
    }
    public static string ExecuteSQLScalar(string sql, string BaglantiCumlesi, string VeritabaniTipi)
    {
        string outPut = string.Empty;
        DataTable dtable = new System.Data.DataTable();
        //try
        //{
        switch (VeritabaniTipi)
        {


            case "MSSQL":
                try
                {
                    SqlConnection connSQL = new SqlConnection(BaglantiCumlesi);
                    connSQL.Open();
                    SqlCommand cmd = new SqlCommand(sql, connSQL);
                    outPut = (String)cmd.ExecuteScalar();
                    connSQL.Dispose();
                    connSQL.Close(); 
                    break;
                }
                catch (Exception )
                {
                    break;
                }
                
               
                   
            case "Oracle":
                   
                break;
        }
        //}
        //catch (Exception ex)
        //{

        //    Functions.Dosya.Logger("Hatalı İşlem :" +ex.Message+ Environment.NewLine + "SQL:" + sql);
        //}

        return outPut;
     

    }
    public static string SPCommand(string ProcedureName, List<SqlParameter> parametreler,string OutputValName,string baglantiCumlesi, string VeritabaniTipi) {
        //foreach (var item in parametreler)
        //{
        //    Functions.Dosya.Logger(item.ParameterName + " :" + item.Value);

        //}
        switch (VeritabaniTipi)
        {
            
            case "MSSQL":
                //try
                //{
                    SqlConnection connSQL = new SqlConnection(baglantiCumlesi);
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.Connection = connSQL;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = ProcedureName;

                 

                    sqlcmd.Parameters.AddRange(parametreler.ToArray());
                    sqlcmd.CommandTimeout = 300;
                    connSQL.Open();
                    sqlcmd.ExecuteReader();
                 

                    string ReturnValue = (string)sqlcmd.Parameters[OutputValName].Value; /*Procedure dönen veriyi alıyoruz.*/
                    if (connSQL.State == ConnectionState.Open)
                    {
                        connSQL.Close();
                    }
                    return ReturnValue;

                //}
                //catch (Exception ex)
                //{
                   
                    
                //  return  ex.Message;
                //}
                
                
                
            default:

                return string.Empty;

        }

      

    }
}
}