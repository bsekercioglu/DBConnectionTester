﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DbConnTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string baglantiCumlesi = String.Format("Data Source={0};Initial Catalog={1};User ID={2};password={3}",txtDS.Text.Trim(),txtCatalog.Text.Trim(),txtUser.Text.Trim(),txtPass.Text.Trim());

        

            try
            {
                using (var connection = new SqlConnection(baglantiCumlesi))
                {
                    connection.Open();
                    lblDurum.Text = @"Durum: BAŞARILI";
                    lblDurum.ForeColor = Color.Green;
                 
                }
            }
            catch (Exception)
            {
                lblDurum.Text = @"Durum: BAŞARISIZ";
                lblDurum.ForeColor = Color.Red;
            }



        }

        private void txtPass_MouseDown(object sender, MouseEventArgs e)
        {
            txtPass.UseSystemPasswordChar = false;

        }

        private void txtPass_MouseUp(object sender, MouseEventArgs e)
        {
            txtPass.UseSystemPasswordChar = true;
        }

        private void lblDurum_Click(object sender, EventArgs e)
        {
            lblDurum.Text = "Durum : ";
        }

    }
}
